<?php
/**
 * Plugin Name: Dm3Widgets
 * Author: incrediblebytes
 * Author URI: http://incrediblebytes.com
 * Description: Adds custom widgets.
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Version: 1.2.0
 *
 * @package Dm3Widgets
 */

/*
Copyright (C) 2015 http://incrediblebytes.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'DM3WIDGETS_PATH', plugin_dir_path( __FILE__ ) );

require_once 'widgets/flickr.php';
require_once 'widgets/posts.php';
require_once 'widgets/twitter.php';
require_once 'widgets/contacts.php';

/**
 * Register widgets.
 */
function dm3widgets_register() {
	register_widget( 'Dm3WidgetsFlickr' );
	register_widget( 'Dm3WidgetsPosts' );
	register_widget( 'Dm3WidgetsTwitter' );
	register_widget( 'Dm3WidgetsContacts' );
}
add_action( 'widgets_init', 'dm3widgets_register' );

/**
 * Load text domain.
 */
function dm3widgets_textdomain() {
	load_plugin_textdomain( 'dm3-widgets', false, 'dm3-widgets/languages' );
}
add_action( 'plugins_loaded', 'dm3widgets_textdomain' );

/**
 * Update widgets when a shared term is split.
 *
 * @param int $term_id
 * @param int $new_term_id
 * @param int $term_taxonomy_id
 * @param string $taxonomy
 */
function dm3widgets_split_shared_term( $term_id, $new_term_id, $term_taxonomy_id, $taxonomy ) {
	if ( 'category' == $taxonomy ) {
		$instances = get_option( 'widget_dm3widgetsposts' );

		if ( ! empty( $instances ) && is_array( $instances ) ) {
			foreach ( $instances as $key => $instance ) {
				if ( $instance['category'] == $term_id ) {
					$instances[ $key ]['category'] = $new_term_id;
				}
			}

			update_option( 'widget_dm3widgetsposts', $instances );
		}
	}
}
add_action( 'split_shared_term', 'dm3widgets_split_shared_term', 10, 4 );
