<?php
/**
 * Twitter widget.
 * 
 * @package Dm3Widgets
 * @since Dm3Widgets 1.0
 * @version 1.1
 */
if ( ! defined( 'ABSPATH' ) ) exit;

class Dm3WidgetsTwitter extends WP_Widget {
	protected $_enable_cache = false;

	/**
	 * Register widget.
	 */
	function __construct() {
		parent::__construct(
			'Dm3WidgetsTwitter', // ID
			'Twitter (Dm3Widgets)', // Name
			array(
				'classname'   => 'dm3-widgets-twitter-widget',
				'description' => __( 'Twitter Widget', 'dm3-widgets' ),
			)
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( $instance, array(
			'widget_code' => '',
		) );

		echo '<div class="dm3-widgets-twitter-widget">';
		echo $instance['widget_code'];
		echo '</div>';
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		if ( current_user_can( 'unfiltered_html' ) ) {
			$instance['widget_code'] = $new_instance['widget_code'];
		} else {
			$instance['widget_code'] = wp_kses_post( $new_instance['widget_code'] );
		}

		return $instance;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( $instance, array(
			'widget_code' => '',
		) );
		?>
		<p>
			<label><?php _e( 'Widget Code', 'dm3-widgets' ); ?></label>
			<textarea name="<?php echo esc_attr( $this->get_field_name( 'widget_code' ) ); ?>" class="widefat" cols="20" rows="5"><?php echo esc_textarea( $instance['widget_code'] ); ?></textarea>
		</p>
		<?php
	}
}
